package pl.exaco.internship.android.weatherdemo.model

import com.google.gson.annotations.SerializedName

data class CityFiveWeather(
        @SerializedName("dt") val dt: Int = 0,
        @SerializedName("main") val main: Main? = null,
        @SerializedName("weather") val cityWeather: List<Weather>? = null,
        @SerializedName("clouds") val clouds: Clouds? = null,
        @SerializedName("wind") val wind: Wind? = null,
        @SerializedName("sys") val sys: Sys? = null,
        @SerializedName("dt_txt") val name: String? = null
)