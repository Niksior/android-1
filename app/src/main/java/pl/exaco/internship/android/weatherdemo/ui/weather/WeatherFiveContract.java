package pl.exaco.internship.android.weatherdemo.ui.weather;

import java.util.List;

import pl.exaco.internship.android.weatherdemo.model.CityFiveWeather;

public class WeatherFiveContract {
    interface Presenter {
        void getWeather();
    }

    interface View {
        void onSuccess(List<CityFiveWeather> data);

        void onFailure();
    }
}
