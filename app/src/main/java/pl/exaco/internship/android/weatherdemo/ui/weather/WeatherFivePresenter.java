package pl.exaco.internship.android.weatherdemo.ui.weather;

import java.util.List;

import pl.exaco.internship.android.weatherdemo.model.CityFiveWeather;
import pl.exaco.internship.android.weatherdemo.service.IServiceFactory;
import pl.exaco.internship.android.weatherdemo.service.IWeatherManager;
import pl.exaco.internship.android.weatherdemo.service.RequestCallback;

public class WeatherFivePresenter implements WeatherFiveContract.Presenter {
    private final WeatherFiveContract.View view;
    private final IWeatherManager weatherManager;
    private final int id;

    public WeatherFivePresenter(WeatherFiveContract.View view, IServiceFactory serviceFactory, int id) {
        this.view = view;
        this.weatherManager = serviceFactory.getWeatherManager();
        this.id = id;
    }

    @Override
    public void getWeather() {
        weatherManager.getWeatherFive(id, new RequestCallback<List<CityFiveWeather>>() {
            @Override
            public void onSuccess(List<CityFiveWeather> data) {
                view.onSuccess(data);
            }

            @Override
            public void onError(Throwable throwable) {
                view.onFailure();
            }
        });
    }
}
